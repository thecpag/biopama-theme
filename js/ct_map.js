var indicatorAjaxRequest = '';

jQuery(document).ready(function($) {

    $( "#block-indicatorcards" ).sortable({
        handle: ".grab",
        //scroll: true,
        axis: "y",
        scrollSpeed: 10
    });

    $(".indicator-actions-icon[aria-label='Minimise all layers']").tooltip();
    $(".indicator-actions-icon[aria-label='Close all layers']").tooltip();

    $( "#block-indicatorcards" ).on( "sortbeforestop", function( event, ui ) {
        $(".indicator-actions-icon").tooltip("hide");
        var nid = $(ui.item).find(".nid").text().trim();
        //console.log($(ui.item).find(".map-layer-id").first().text());
        var thisID = ui.item[0].id;
        $().moveMapLayers(nid, "up"); 
        var sortedIDs = $( "#block-indicatorcards" ).sortable( "toArray" );

        if (thisID == sortedIDs[0]) { //if it's first, it's at the top (move up)
            $().moveMapLayers(nid, "up"); 
        } else { 
            //if it's not first, check the immediate following layer and move behind it. 
            var nextid = $("#"+thisID).prev().find(".nid").text().trim();
            var layerid = $("#"+thisID).prev().find(".map-layer-id").first().text().trim();
            $().moveMapLayers(nid, layerid+"-b10p4m4#id-"+nextid); 
        }
    } );

    $("#block-indicatorcards").on('shown.bs.collapse', function (e) {
        e.stopPropagation();
        $(this).closest("#block-indicatorcard").find("i.fa-window-maximize").first().removeClass("fa-window-maximize").addClass("fa-window-minimize");
        var tooltip = bootstrap.Tooltip.getInstance(".indicator-actions-icon[aria-label='Minimise all layers']");
        tooltip.setContent({ '.tooltip-inner': 'Minimise all layers' });
    });
    $("#block-indicatorcards").on('hide.bs.collapse', function (e) {
        e.stopPropagation();
        $(this).closest("#block-indicatorcard").find("i.fa-window-minimize").first().removeClass("fa-window-minimize").addClass("fa-window-maximize")
        var tooltip = bootstrap.Tooltip.getInstance(".indicator-actions-icon[aria-label='Minimise all layers']");
        tooltip.setContent({ '.tooltip-inner': 'Maximize all layers' });
    });

    Drupal.behaviors.IndicatorCard = {
		attach: function (context, settings) {
            $(context).find(".indicator-content-wrapper").once("add-indicator-behaviours").each(function () {
                //console.log("test");
                //addIndicator();
                //Drupal.attachBehaviors($(".indicator-content-wrapper").get(0));
                var nid = $(this).find(".nid").text().trim();
                var layerID = $(this).find(".layer_nid").text().trim();
                
                $( "#block-indicatorcard" ).draggable({
                    handle: ".grab-all",
                    containment: "#map-container"
                });

                
                $("#collapseCard"+nid).on('shown.bs.collapse', function (e) {
                    e.stopPropagation();
                    $("#id-"+nid).find(".mini-title").addClass("d-none");
                    $("#id-"+nid).find("i.fa-window-maximize").removeClass("fa-window-maximize").addClass("fa-window-minimize")
                    var tooltip = bootstrap.Tooltip.getInstance("#id-" + nid + " .indicator-actions-icon[aria-label='Minimise']");
                    tooltip.setContent({ '.tooltip-inner': 'Minimize' });
                });
                $("#collapseCard"+nid).on('hide.bs.collapse', function (e) {
                    e.stopPropagation();
                    $("#id-"+nid).find(".mini-title").removeClass("d-none");
                    $("#id-"+nid).find("i.fa-window-minimize").removeClass("fa-window-minimize").addClass("fa-window-maximize")
                    var tooltip = bootstrap.Tooltip.getInstance("#id-" + nid + " .indicator-actions-icon[aria-label='Minimise']");
                    tooltip.setContent({ '.tooltip-inner': 'Maximize' });
                });

                const alertPlaceholder = document.getElementById('AlertPlaceholder'+layerID);
        
                const alert = (message, type) => {
                    const wrapper = document.createElement('div')
                    wrapper.innerHTML = [
                      `<div class="alert alert-${type} alert-dismissible fade show" role="alert">`,
                      `   <div>${message}</div>`,
                      '   <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>',
                      '</div>'
                    ].join('')
                  
                    alertPlaceholder.append(wrapper)
                }
        
                const downloadStatisticsTrigger = document.getElementById('downloadStatistics'+layerID);
                if (downloadStatisticsTrigger) {
                    downloadStatisticsTrigger.addEventListener('click', () => {
                        alert('Statistics not available for this layer.', 'warning');
                    })
                }

                const layerUpTrigger = document.getElementById('layerUp'+layerID);
                if (layerUpTrigger) {
                    layerUpTrigger.addEventListener('click', () => {
                        $(this).closest(".card.indicator-card").detach().prependTo("#block-indicatorcards");
                        //moveLayers(nid, "up")
                        $().moveMapLayers(nid, "up"); 
                    })
                }

                const layerDownTrigger = document.getElementById('layerDown'+layerID);
                if (layerDownTrigger) {
                    layerDownTrigger.addEventListener('click', () => {
                        $(this).closest(".card.indicator-card").detach().appendTo("#block-indicatorcards");
                        //moveLayers()
                        $().moveMapLayers(nid, "down"); 
                    })
                }
        
            });
		}
	};



    $(".check-filter").on('click', function (event) {
        event.preventDefault();
        console.log($("label", this).text().trim())
        if ($("label", this).text().trim() == "European Commission Datasets"){
            if ($("input", this).prop("checked")) {
                $("input", this).prop('checked', false);
                $("#menu-indicator-cards .source-ec-jrc").parent().addClass("d-none");
            } else {
                $("input", this).prop('checked', true);
                $("#menu-indicator-cards .source-ec-jrc").parent().removeClass("d-none");
            }
        }

        if ($("label", this).text().trim() == "Third Parties Datasets"){
            if ($("input", this).prop("checked")) {
                $("input", this).prop('checked', false);
                $("#menu-indicator-cards .indicator-card:not(.source-ec-jrc)").parent().addClass("d-none");
            } else {
                $("input", this).prop('checked', true);
                $("#menu-indicator-cards .indicator-card:not(.source-ec-jrc)").parent().removeClass("d-none");
            }   
        }
        
    });

    var map;
    if($("#map-container").length){ //we are checking if the map div is the one from the CTT page
        map = $().createMap("map-container");

        $().addMapControls(map, "satelliteToggle");
        $().addMapControls(map, "zoom");
        $().addMapControls(map, "hoveredAreaLabelBlock");
        $().addMapControls(map, "paPolyFillControl");
        $().addMapControls(map, "loaderControl");
        $().addMapControls(map, "fullScreen");
        $().addMapControls(map, "navigation");	
    
        $().addMapLayerBiopamaSources(map);
            
        //$().addMapLayer(map, "biopamaGaulEez");
        $().addMapLayer(map, "biopamaCountries");
        
        $().addMapLayer(map, "satellite");
        $().addMapLayer(map, "nan-layers");

        
        //var mask = L.tileLayer('https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/africa_platform:world_flat_no_africa_no_eez_@EPSG:900913@png/{z}/{x}/{y}.png', { tms: true, zIndex: 40, opacity: 1 }).addTo(map)

        // var popupOptions = {
        //     countryLink: '/ct/country/',
        //     paLink: '/ct/pa/',
        // }
        // $().addMapLayerInteraction(map, popupOptions);

    } else { // if it's not the CTT div we can assume we are on the Country/PA page and use that map instead.
        map = mymap;
    };
	
	$('input').each(function(i){
	  if(this.id){
		this.id = this.id+i;
		$(this).closest('form').addClass(this.id);
	  }
	});

	map.on('moveend', function () {
		//this flag can only be true in this case if the Protected Area has been changed to one in a different country from the search
		if (countryChanged === 1){	
			updateCountry();
		}
		if (paChanged === 1){	
			updatePa();
		}
		if (regionChanged === 1){	
			updateRegion();
		}
	});
	
	map.on('load', function () {
        if (window.location.search.indexOf('?search=') > -1) { // check if we got here with a search term in the URL (to open an indicator by default)
            $("#menu-indicator-cards .indicator-card:first .menu-open-indicator").click(); //find the open indicator button of the first search result and click it.
        }
		$('body').toggleClass('loaded').delay( 500 ).queue(function() {
		  $('.mapboxgl-ctrl.ajax-loader').toggle(false);
		  mapPostLoadOptions();
		  $( this ).dequeue();
		});

        const layers = map.getStyle().layers;
        let firstSymbolId;
        for (const layer of layers) {
            if (layer.type === 'symbol') {
                firstSymbolId = layer.id;
                break;
            }
        }
        //console.log(layers)
        var url_eez = "https://geospatial.jrc.ec.europa.eu/geoserver/africa_platform/wms?SERVICE=WMS&REQUEST=GetMap&VERSION=1.1.1&LAYERS=africa_platform%3Agaul_eez_dissolved&STYLES=africa_platform_mask_country2&FORMAT=image%2Fpng&TRANSPARENT=true&HEIGHT=256&WIDTH=256&SRS=EPSG:3857&BBOX={bbox-epsg-3857}"
        map.addLayer({
            'id': 'eez',
            'type': 'raster',
            'source': {
                'id': 'eez',
                'type': 'raster',
                'tiles': [url_eez],
                'tileSize': 256,
                'scheme': 'xyz',
            },
            'paint': {"raster-opacity" : 0.7}
        }, 'countrySelected');
        var url_eez2 = "https://geospatial.jrc.ec.europa.eu/geoserver/africa_platform/wms?SERVICE=WMS&REQUEST=GetMap&VERSION=1.1.1&LAYERS=africa_platform%3Agaul_eez_dissolved&STYLES=africa_platform_mask_country2&FORMAT=image%2Fpng&TRANSPARENT=true&HEIGHT=256&WIDTH=256&SRS=EPSG:3857&CQL_FILTER=iso2%20LIKE%20%27WWW%27&BBOX={bbox-epsg-3857}"
        map.addLayer({
            'id': "eez2",
            'type': 'raster',
            'source': {
                'id': "eez2",
                'type': 'raster',
                'tiles': [url_eez2],
                'tileSize': 256,
                'scheme': "xyz",
            },
            'paint': {"raster-opacity" : 1}
        }, 'countrySelected');
        var url_afica_mask = "https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/africa_platform:world_flat_no_africa_no_eez_@EPSG:900913@png/{z}/{x}/{y}.png"
        map.addLayer({
            'id': 'afica_mask',
            'type': 'raster',
            'source': {
                'id': 'afica_mask',
                'type': 'raster',
                'tiles': [url_afica_mask],
                'tileSize': 256,
                'scheme': 'tms',
            },
            'paint': {"raster-opacity" : 1}
        }, 'countrySelected');
        // var url_ap_country_stats = "https://geospatial.jrc.ec.europa.eu/geoserver/africa_platform/wms?SERVICE=WMS&REQUEST=GetMap&VERSION=1.1.1&LAYERS=africa_platform%3Aap_country_stats&STYLES=white_polygon_t&FORMAT=image%2Fpng&TRANSPARENT=true&HEIGHT=256&WIDTH=256&SRS=EPSG:3857&BBOX={bbox-epsg-3857}";
        // map.addLayer({
        //     'id': 'ap_country_stats',
        //     'type': 'raster',
        //     'source': {
        //         'id': 'ap_country_stats',
        //         'type': 'raster',
        //         'tiles': [url_ap_country_stats],
        //         'tileSize': 256,
        //         'scheme': 'xyz',
        //     },
        //     'paint': {"raster-opacity" : 1}
        // }, 'countrySelected');


        var url_dotted = "https://geospatial.jrc.ec.europa.eu/geoserver/africa_platform/wms?SERVICE=WMS&REQUEST=GetMap&VERSION=1.1.1&LAYERS=africa_platform%3Adotted_ws&FORMAT=image%2Fpng&TRANSPARENT=true&HEIGHT=256&WIDTH=256&ZINDEX=35&SRS=EPSG:3857&BBOX={bbox-epsg-3857}"
        map.addLayer({
            'id': "dotted",
            'type': 'raster',
            'source': {
                'id': "eez",
                'type': 'raster',
                'tiles': [url_dotted],
                'tileSize': 256,
                'scheme': "xyz",
            },
            'paint': {}
        }, 'countrySelected');
        // map.moveLayer("country-label", "dotted");
        // map.moveLayer("state-label", "dotted");
        
	});

	function mapPostLoadOptions() {
		map.setMinZoom(1.4);
		map.setMaxZoom(16);
	}

});

function getRestResults(){
    jQuery().insertBiopamaLoader(".indicator-chart"); 
    firstChartRun = 1;
    var dataCountry = 1;
    //delete any errors that might be up, if they persist, they will be re-added
    jQuery( ".rest-error" ).empty();
    //if the chosen indicator has countries attached to it we highlight them, and mask the ones not included.
    if (selData.info.countries != ''){
        thisMap.setFilter("CountriesBadMask", buildFilter(selData.info.countries, '!in', 'iso3'));
        thisMap.setFilter("CountriesGoodMask", buildFilter(selData.info.countries, 'in', 'iso3'));
        thisMap.setLayoutProperty("CountriesGoodMask", 'visibility', 'visible');
        thisMap.setLayoutProperty("CountriesBadMask", 'visibility', 'visible');
    }
    if (selData.data.countries.length > 0){
        if (selData.data.countries.indexOf(selSettings.ISO3) > -1){
            // a country or countries have been set and the current country is in the set
            dataCountry = 1;
        } else {
            //a country or countries have been set and the currently selected country is not one of them
            dataCountry = 0;
        }
    }
    if (dataCountry == 1){

        //generate url by replacing tokens that might be in it.
        var indicatorURL = '';
        indicatorURL = jQuery().biopamaReplaceTokens(selData.chart.RESTurl, selSettings);  
        
        indicatorAjaxRequest = jQuery.ajax({
            url: indicatorURL,
            dataType: 'json',
            success: function(d) {
                jQuery().removeBiopamaLoader(".indicator-chart"); 
                if (d.hasOwnProperty("records")){ //from a JRC REST Service
                    selData.chart.RESTResults = d.records;
                    if (d.metadata.recordCount == 0) {
                        //we create a card, but tell it that the response was empty (error 2)
                        getChart(2);
                    } else {
                        //the 0 means there was no error
                        getChart(0);
                    }
                } else if (d.hasOwnProperty(selData.chart.RESTdataContext)){ //single value passed, found in root, good to go.
                    selData.chart.RESTResults = d[selData.chart.RESTdataContext];
                    getChart(0);
                } else if (selData.chart.RESTdataContext !== ''){
                    if (indicatorURL.includes("observatoire-comifac.net")){
                        var OFACCountryIso3 = ["CMR","COD","GAB","TCD","GNQ","RWA","BDI","STP","CAF","COG"];
                        var OFACCountryNames = ["Cameroon","Congo (the Democratic Republic of the)","Gabon","Chad","Equatorial Guinea","Rwanda","Burundi","São Tomé and Príncipe","Central African Republic","Republic of the Congo"];
                        var countryArray = jmespath.search(d, 'data.*.eligible_country[]');

                        //needed as the ofac-comifac services change between an array and an obj.
                        if(typeof countryArray[0] !== 'string'){
                            countryArray = jQuery().getKeys(countryArray[0]); 
                        }

                        var countryNamesIn = [];
                        var countryMapIn = ['in', 'iso3'];
                        var countryNamesOut = [];
                        var countryMapOut = ['in', 'iso3'];
                        
                        OFACCountryIso3.forEach(function (value, index) {
                            if (countryArray.includes(value)) {
                                countryNamesIn.push(OFACCountryNames[index]);
                                countryMapIn.push(OFACCountryIso3[index]);
                            } else {
                                countryNamesOut.push(OFACCountryNames[index]);
                                countryMapOut.push(OFACCountryIso3[index]);
                            }
                        });

                        var countryNamesInString = countryNamesIn.join(", ");
                        var countryNamesOutString = countryNamesOut.join(", ");

                        thisMap.setFilter("CountriesGreenMask", countryMapIn);
                        thisMap.setLayoutProperty("CountriesGreenMask", 'visibility', 'visible');
                        thisMap.setFilter("CountriesRedMask", countryMapOut);
                        thisMap.setLayoutProperty("CountriesRedMask", 'visibility', 'visible');
                        if (countryNamesInString){
                            jQuery( '<p style="color: #8fc04f;"><b>Pays atteignant l\'objectif: </b>'+ countryNamesInString + '</p>' ).insertAfter( "#indicator-chart-regional" );
                        } else {
                            jQuery( '<p style="color: #858585;"><b>Aucun pays n\'a atteint cet objectif.</b></p>' ).insertAfter( "#indicator-chart-regional" );
                        }
                    
                    }
                    //var match1 = jsonPath(d, selData.chart.RESTdataContext); jsonPath works too, but JMES has better documentation
                    var match1 = jmespath.search(d, selData.chart.RESTdataContext);
                    //console.log(match1);
                    selData.chart.RESTResults = match1;
                    getChart(0);
                } else {
                    selData.chart.RESTResults = d;
                    getChart(0);
                }
            },
            error: function() {
                jQuery().removeBiopamaLoader(".indicator-chart"); 
                console.log("ERROR")
                //we create a card, but tell it that there was a general error (error 1)
                //todo - expand error codes to tell user what went wrong.
                getChart(1);
            }
        });
    } else {
        jQuery().removeBiopamaLoader(".indicator-chart"); 
        //we run the get chart function, only to show the user that a different country must be selected
        getChart(3);
    }
    //AKP
}